//
//  BaseListController.swift
//  DiscoverMovies
//
//  Created by Ualikhan on 3/18/20.
//  Copyright © 2020 Ualikhan Sabden. All rights reserved.
//

import UIKit
class BaseListController: UICollectionViewController {
    
    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
