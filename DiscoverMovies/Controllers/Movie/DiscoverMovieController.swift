//
//  DiscoverMovieController.swift
//  DiscoverMovies
//
//  Created by Ualikhan on 3/18/20.
//  Copyright © 2020 Ualikhan Sabden. All rights reserved.
//

import UIKit

class DiscoverMovieController: BaseListController, UICollectionViewDelegateFlowLayout {
    
    fileprivate let cellId = "cellId"
    fileprivate let footerId = "footerId"
    fileprivate let moviesURL = "https://api.themoviedb.org/3/discover/movie"
    
    fileprivate var currentPageNumber = 1
    
    var results = [Results]()
    var favListArray: NSMutableArray = []
    
    fileprivate var refresher: UIRefreshControl!
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.object(forKey: "favList") != nil {
            favListArray = NSMutableArray.init(array: UserDefaults.standard.object(forKey: "favList") as! NSMutableArray)
        }

        collectionView.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.94, alpha: 1)
        
        collectionView.register(MovieCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(LoadingFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerId)
        pullRefresh()
        fetchData()
    }
    
    // MARK:- Pull to refresh
    fileprivate func pullRefresh() {
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.collectionView!.refreshControl = refresher
        self.refresher.tintColor = UIColor.gray
        self.refresher.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    @objc func refreshData() {
        self.collectionView!.refreshControl?.beginRefreshing()
        self.fetchData()
        stopRefresher()
    }
    
    func stopRefresher() {
        self.collectionView!.refreshControl?.endRefreshing()
    }
    
    //MARK:- Fetch Data
    fileprivate func fetchData() {
        
        Service.shared.fetchGenericJSONData(urlString: moviesURL) { (moviesResult: MoviesResult?, err) in
            
            if let err = err {
                print("Failed to paginate data:", err)
                return
            }
            
            self.results = moviesResult?.results ?? []
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    
    //MARK:- Collection View setup
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath)
        return footer
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let height: CGFloat = isDonePaginating ? 0 : 100
        return .init(width: view.frame.width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    
    var isPaginating = false
    var isDonePaginating = false
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! MovieCell
        let movie = results[indexPath.item]
        cell.configure(with: movie)
        
        if favListArray.contains(cell.nameLabel.text!) {
            cell.favButton.setImage(UIImage(named: "star_selected")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }else{
            cell.favButton.setImage(UIImage(named: "star_black")?.withRenderingMode(.alwaysOriginal), for: .normal)
        }

        cell.favButton.tag = indexPath.item
        cell.favButton.addTarget(self, action: #selector(addToFav), for: .touchUpInside)

        
        if indexPath.item == results.count - 1 && !isPaginating {
            print("fetch more data")
            
            isPaginating = true
            
            Service.shared.fetchGenericJSONData(urlString: moviesURL) { (movieResult: MoviesResult?, err) in
                
                if let err = err {
                    print("Failed to paginate data:", err)
                    return
                }
                
                if movieResult?.results.count == 0 {
                    self.isDonePaginating = true
                }
                
                // задержка
                sleep(2)
                
                self.results += movieResult?.results ?? []
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                self.isPaginating = false
            }
        }
        
        return cell
    }
    
    @objc func addToFav(sender: UIButton) {
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: sender.tag, section: 0)) as! MovieCell
        guard let name = cell.nameLabel.text else { return }
        if favListArray.contains(name) {
            favListArray.remove(name)
        }else{
            favListArray.add(name)
        }
        
        collectionView.reloadData()
        
        UserDefaults.standard.set(favListArray, forKey: "favList")
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = DetailMovieController()
        let result = results[indexPath.item]
        vc.result = result
        vc.favListArray = favListArray
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Collection View Layouts
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 20, left: 0, bottom: 20, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 20, height: 180)
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    //MARK:- LifeCycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Discover Movies"
        navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        navigationController?.navigationBar.shadowImage = nil
        navigationController?.navigationBar.prefersLargeTitles = true
        collectionView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = nil
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    
}

