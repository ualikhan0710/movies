//
//  GenreModel.swift
//  DiscoverMovies
//
//  Created by Ualikhan on 3/19/20.
//  Copyright © 2020 Ualikhan Sabden. All rights reserved.
//

import Foundation

protocol GenreViewModelProtocol: class {
    var genres: [Genres] { get }
}

class GenreModel: GenreViewModelProtocol {
    var genres = [Genres]()
    
    init() {
        retrieveGenres()
    }
    
    func retrieveGenres() {
        guard let url = Bundle.main.url(forResource: "genre", withExtension: "json") else { return }
        do {
            let data = try Data(contentsOf: url)
            genres = try JSONDecoder().decode(GenresResponse.self, from: data).genres
            
        } catch {
            debugPrint(error.localizedDescription)
        }
    }
    
}
