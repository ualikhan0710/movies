//
//  GenresResponse.swift
//  DiscoverMovies
//
//  Created by Ualikhan on 3/19/20.
//  Copyright © 2020 Ualikhan Sabden. All rights reserved.
//

import Foundation

struct GenresResponse: Decodable {
    let genres: [Genres]
}

struct Genres: Decodable {
    let id: Int
    let name: String
}
