//
//  MoviesResult.swift
//  DiscoverMovies
//
//  Created by Ualikhan on 3/19/20.
//  Copyright © 2020 Ualikhan Sabden. All rights reserved.
//

import Foundation

struct MoviesResult: Codable {
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let results: [Results]
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results = "results"
    }
    
}

struct Results: Codable {
    let popularity: Double
    let voteCount: Int
    let posterPath: String?
    let id: Int
    let backdropPath: String?
    let originLan: String?
    let originTitle: String?
    let genreId: [Int]
    let title: String
    let voteAverage: Double
    let overview: String
    let releaseDate: String
    
    enum CodingKeys: String, CodingKey {
        case popularity, id, title, overview
        case voteCount = "vote_count"
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case originLan = "original_language"
        case originTitle = "original_title"
        case genreId = "genre_ids"
        case voteAverage = "vote_average"
        case releaseDate = "release_date"
    }
    
}
