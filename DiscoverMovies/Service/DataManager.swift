//
//  DataManager.swift
//  DiscoverMovies
//
//  Created by Ualikhan on 3/19/20.
//  Copyright © 2020 Ualikhan Sabden. All rights reserved.
//

import Foundation

class DataManager {
    
    static let shared = DataManager()
    
    private let userDefaults = UserDefaults()
    
    func saveFavouriteStatus(for movieName: String, with status: Bool) {
        userDefaults.set(status, forKey: movieName)
    }
    
    func loadFavouriteStatus(for movieName: String) -> Bool {
        return userDefaults.bool(forKey: movieName)
    }
}

