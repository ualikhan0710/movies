//
//  Service.swift
//  DiscoverMovies
//
//  Created by Ualikhan on 3/19/20.
//  Copyright © 2020 Ualikhan Sabden. All rights reserved.
//

import Foundation

class Service {
    
    static let shared = Service()
    
    func fetchGenericJSONData<T: Decodable>(urlString: String , completion: @escaping (T?, Error?) -> () ){
        
        guard let url = URL(string: urlString) else {return}
        
        var request = URLRequest(url: url)
        request.setValue("api_key", forHTTPHeaderField: "08fb5a6fdc0b0999e99978c98488d398")
        request.setValue("application/json;charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIwOGZiNWE2ZmRjMGIwOTk5ZTk5OTc4Yzk4NDg4ZDM5OCIsInN1YiI6IjVlNzA5ZjNmMzU3YzAwMDAxOTQ2Yjc0MyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.0SjqN_8WZSOwn1b6CFv-gTBxZS2XsnIrmXZQAx-8M5M", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, response, err) in
            
            if let err = err {
                completion(nil, err)
                return
            }
            
            do {
                let objects = try JSONDecoder().decode(T.self, from: data!)
                completion(objects,nil)
            } catch {
                completion(nil, error)
                print("Failed to decode:",error)
            }
            }.resume()
    }
    
    
    
}
