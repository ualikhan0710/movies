//
//  LoadingFooter.swift
//  DiscoverMovies
//
//  Created by Ualikhan on 3/18/20.
//  Copyright © 2020 Ualikhan Sabden. All rights reserved.
//

import UIKit

class LoadingFooter: UICollectionReusableView {
    
    lazy var aiv: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(style: .whiteLarge)
        aiv.color = .darkGray
        return aiv
    }()
    
    lazy var label: UILabel = {
        let l = UILabel()
        l.text = "Loading More..."
        l.font = .systemFont(ofSize: 16)
        l.textAlignment = .center
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        aiv.startAnimating()
        
        let stackView = UIStackView(arrangedSubviews: [aiv, label], axis: .vertical, distribution: .fillEqually, spacing: 8)
        
        addSubview(stackView)
        stackView.centerInSuperview(size: .init(width: 200, height: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
